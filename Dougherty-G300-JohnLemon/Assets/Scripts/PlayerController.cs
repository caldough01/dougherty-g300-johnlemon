using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public TextMeshProUGUI tokenCount;
    public GameObject winTextObject;
    public GameObject loseTextObject;

    public GameObject instructions;

    public int count;

    public Timer time;

    private float fixedDeltaTime;

    void Awake()
    {
        this.fixedDeltaTime = Time.fixedDeltaTime;
    }

    // Start is called before the first frame update
    void Start()
    {

        SetTokenCount();
        winTextObject.SetActive(false);

        instructions.SetActive(true);
        //freeze the world
        Time.timeScale = 0f;

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.Space))
        {
            instructions.SetActive(false);
            //unfreeze the world
            Time.timeScale = 1f;
        }
        
    }

    void SetTokenCount()
    {
        tokenCount.text = "Tokens: " + count.ToString();
    }
   
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetTokenCount();
        }

       /* if (other.gameObject.CompareTag("Finish") && (count >= 8) && (time.timeValue > 0))
        {
            winTextObject.SetActive(true);

            time.timerRunning = false;
        }
       */
    }
}
