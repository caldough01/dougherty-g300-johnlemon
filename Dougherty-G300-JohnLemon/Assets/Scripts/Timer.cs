using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{

    public float timeValue = 120f;
    public float currentTimer = 0f;
    public bool timerRunning;

    public TextMeshProUGUI timeText;
    public GameObject loseTextObject;

    // Start is called before the first frame update
    void Start()
    {
        StartTimer();
        SetTimeText();

        loseTextObject.SetActive(false);
    }

    public void StartTimer()
    {
        currentTimer = timeValue;
        timerRunning = true;
    }

    void SetTimeText()
    {
        timeText.text = "Time Left: " + Mathf.Round(timeValue) + " sec.";
    }

    // Update is called once per frame
    void Update()
    {
        currentTimer = timeValue;
        SetTimeText();

        if (timerRunning)
        {
            if (timeValue > 0)
            {
                timeValue -= Time.deltaTime;
            }
            else
            {
                timerRunning = false;
                timeValue = 0;

            

                //freeze world
                loseTextObject.SetActive(true);
            }
        }


    }
}
